<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    function tentukan_nilai($number)
    {
        if($number >= 98) {
            echo "sangat Baik! <br>";
        }elseif ($number >= 76) {
            echo "Baik! <br>";
        }elseif ($number >= 67) {
            echo "Cukup! <br>";
        }elseif ($number >= 43) {
            echo "Kurang! <br>";
        }else {
            echo "Semangat! <br>";
        }
    }

        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

    ?>
</body>
</html>